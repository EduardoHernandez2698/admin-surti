
import Vue from 'vue'
import Vuex from 'vuex'
import VueJwtDecode from 'vue-jwt-decode'

// import rutas    from './router'
import router   from '@/router'

import Login     from '@/modules/Login/Login'
import Registro  from '@/modules/Login/Registro'	
import productos from '@/modules/productos'
import tema      from '@/modules/tema'

Vue.use(Vuex,VueJwtDecode)

export default new Vuex.Store({
  state: {
  	usuario: '',
	    token: null,   
	    usuario: null,
	    nivel: null,
	    acuses:'',
	    drawer: true,
	    menu: true,
  },
  mutations: {
  	setToken(state, token){
    	// console.log('recibo token', token)
      state.token= token
    },
    setUsuario (state, usuario){
    	// console.log('serUsuario', usuario)
      state.usuario= usuario
    },
    setNivel (state, nivel){
    	// console.log('setNivel', nivel)
      state.nivel=  nivel
    }

  },
  actions: {
  	// Se manda llamar desde Login.vue
	    guardarToken({commit},token){
	    	// console.log('guardarToken')
	    	// Guardar token
	      commit("setToken", token)
	      // Colocar Token en LocalStorage
	      localStorage.setItem("tlaKey", token)

	     	//Decifrar Usuario
	     	var decode        = VueJwtDecode.decode(token)
	     	var UsuarioActivo = decode.id 
	     	// Hacer commit para guardar Usuario
	     	commit("setUsuario", UsuarioActivo)   // Decodifica el token para sacar usuario
	    },

	    guardarNivel({commit},nivel){
	    	// console.log("gaurdarnivel", nivel)
	    	commit('setNivel', nivel)

	    },
	    
	    autoLogin({commit}){
	    },

	    salir({commit}){
	      // localStorage.removeItem("token")
	      commit("setUsuario", '')
	      commit("setToken", '')
	      localStorage.removeItem("tlaKey")
          //var tokenGen = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjIyNTg4NjgsImlkIjoiYWRtaW4iLCJvcmlnX2lhdCI6MTU2MjI1NTI2OH0.SWGD5aH16otmX_cu-EaQ9OlgnLwV-WxbAuy85LUS1bE';

	      //localStorage.capaKey  = JSON.stringify(tokenGen);
	      router.push({name: 'Login'})
	    }
  },

  getters:{
		traeNomuser(state){
			// console.log('traeNomuser')
			return state.usuario
		},
		traeNivel(state){
			// console.log('traeNivel')
			return state.nivel
		},

	},

	modules:{
		Login,
		Registro,
		productos,
		tema
	}

})

