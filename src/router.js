import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/Login/Home.vue'
import Login from '@/views/Login/Login.vue'

import VueJwtDecode from 'vue-jwt-decode'

import store from './store'
import rutas from './router'
Vue.use(Router)

// export default new Router({

var router = new Router({    
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    // { path: '/',     name: 'login', component: Login},
    // { path: '/home', name: 'home',  component: Home},

    //MODULO DE LOGIN
    { path: '/home', name: 'home' , component: Home, 
      meta: { ADMIN: true, USER: true, libre: true } },
    { path: '/'    , name: 'Login', component: Login , 
      meta: { libre: true }},
    
    { path: '/myperfil', name: 'myperfil', component:()=> import('@/views/Login/MyPerfil.vue'), 
      meta: { ADMIN: true , USER: true}} ,
    { path: '/registro', name: 'registro', component:()=> import('@/views/Login/Registro.vue') ,
      meta: { libre: true }} ,
    { path: '/olvidacontra', name: 'olvidacontra', component:()=> import('@/views/Login/OlvidaContra.vue') ,
      meta: { libre: true }} ,
      
    { path: '/cambiacontra/:id'  , name: 'cambiacontra'  , component:()=> import('@/views/Login/CambiaContra.vue') ,
      meta: { libre: true }},
    {path: '/activarusuario/:id', name: 'activarusuario', component:()=> import('@/views/Login/ActivarUsuario'),
      meta: { libre: true}},


    //Configuración general
    { path: '/tienda',   name: 'tienda',   component:()=> import('@/views/general/Tienda.vue') , 
      meta: { ADMIN: true }},
    { path: '/tema',     name: 'tema',     component:()=> import('@/views/general/Tema.vue') , 
      meta: { ADMIN: true }},
    

    { path: '/envio',    name: 'envio',    component: ()=> import('@/views/general/Envio.vue'),
     meta: { ADMIN: true }},
    { path: '/pago',     name: 'pago',     component: ()=> import('@/views/general/Pago.vue'),
     meta: { ADMIN: true }},
    { path: '/terminos', name: 'terminos', component: ()=> import('@/views/general/Terminos.vue'),
     meta: { ADMIN: true }},

    //utilerias
    { path: '/niveles',  name: 'niveles',   component: ()=> import('@/views/general/Niveles.vue'),
      meta: { ADMIN: true   }},
    { path: '/catusuarios', name: 'catusuarios',  component: ()=> import('@/views/usuarios/CatUsuarios.vue') ,
      meta: { ADMIN: true } },
    { path: '/newusuario', name: 'newusuario',  component: ()=> import('@/views/usuarios/NewUsuario.vue') ,
      meta: { ADMIN: true } },


    //CONSULTA DE DOCUMENTOS
    { path: '/consultadoc', name: 'consultadoc', component: ()=> import('@/views/operaciones/Condocs.vue'),
      meta: { ADMIN: true } },
    { path: '/documento'  , name: 'documento',   component: ()=> import('@/views/operaciones/Documento.vue'),
      meta: { ADMIN: true } },
    { path: '/cotizacion',  name: 'cotizacion',  component: ()=>  import('@/views/operaciones/Cotizacion.vue'),
      meta: { ADMIN: true } },
    { path: '/pedido',      name: 'pedido',      component: ()=>  import('@/views/operaciones/Pedidos.vue') ,
      meta: { ADMIN: true } },
    
    // { path: '/fechas', name: 'fechas',  component: ()=> import('@/views/operaciones/Fechas.vue') }},

    //Artículos 
    { path: '/catproductos', name: 'catproductos', component: ()=>  import('@/views/clasificacion/arts/CatProductos.vue') ,
      meta: { ADMIN: true } },
    { path: '/newproducto', name: 'newproducto', component: ()=>  import('@/views/clasificacion/arts/NewProducto.vue') ,
      meta: { ADMIN: true } },
    // { path: '/cargaimaganes', name: 'cargaimaganes', component: ()=>  import('@/views/clasificacion/arts/CargaImagenes.vue') ,
    //   meta: { ADMIN: true } },
    
    // { path: '/producto.update', name: 'editproducto', component: ()=>  import('@/views/clasificacion/arts/EditProducto.vue') ,
    //  meta: { ADMIN: true } },
              
    // CONSULTA DE INVENTARIOS
    // { path: '/inventario.list', name: 'inventario.list', component: ()=>  import('@/views/clasificacion/arts/ConInv.vue') ,
    //   meta: { ADMIN: true } },
     


    //Herramientas
    { path: '/condocumentos', name: 'condocumentos', component: ()=> import ('@/views/herramientas/Condocumentos.vue'),
      meta: { ADMIN: true } },

    { path: '/conarticulos',  name: 'conarticulos',  component: ()=> import ('@/views/herramientas/ConArticulos.vue') ,
      meta: { ADMIN: true } },
    { path: '/carimagenes',   name: 'carimagenes',   component: ()=> import ('@/views/herramientas/CarImagenes.vue') },
    
    
    //MARKETING
    { path: '/catpromo',      name: 'catpromo', component: ()=> import ('@/views/marketing/promo/Catpromo.vue'),
      meta: {ADMIN: true } },
    { path: '/newpromo',      name: 'newpromo', component: ()=> import ('@/views/marketing/promo/Newpromo.vue'),
      meta: {ADMIN: true } },
    { path: '/editpromo',      name: 'editpromo', component: ()=> import ('@/views/marketing/promo/Editpromo.vue'),
      meta: {ADMIN: true } },       
    { path: '/enviopromo',    name: 'enviopromo',  component: ()=> import ('@/views/herramientas/EnvioPromo.vue') ,
      meta: {ADMIN: true } },
    { path: '/seo',           name: 'seo',   component: ()=> import ('@/views/herramientas/SEO.vue') ,
      meta: {ADMIN: true }},

    { path: '/catpromo2',      name: 'catpromo2', component: ()=> import ('@/views/marketing/promo/Catpromo2.vue'),
      meta: {ADMIN: true } },
    { path: '/newpromo2',      name: 'newpromo2', component: ()=> import ('@/views/marketing/promo/Newpromo2.vue'),
      meta: {ADMIN: true } },



   
    // { path: '/pagarfac   '  , name: 'pagarfac'  , component:()=> import('@/views/FacturasPagos/PagarFac.vue') ,
    //   meta: { ADMIN: true }},
    // CLIENTES
    { path: '/catclientes', name: 'catclientes', component: ()=> import('@/views/clientes/Catclientes.vue') ,
      meta: { ADMIN: true }},
    { path: '/editcliente', name: 'editcliente', component: ()=> import('@/views/clientes/EditCliente.vue') ,
      meta: { ADMIN: true }},
    { path: '/newcliente',  name: 'newcliente',  component: ()=> import('@/views/clientes/Newcliente.vue') ,
      meta: { ADMIN: true }},
    
      // VENDEDORES
    { path: '/catvend', name: 'catvend', component: ()=> import('@/views/vend/Catvend.vue') ,
      meta: { ADMIN: true }},
    { path: '/editvend', name: 'editvend', component: ()=> import('@/views/vend/Editvend.vue') ,
      meta: { ADMIN: true }},
    { path: '/newvend',  name: 'newvend',  component: ()=> import('@/views/vend/Newvend.vue') ,
      meta: { ADMIN: true }},
    


    // CATEGORIAS
    { path: '/catcategorias',    name: 'catcategorias',     component: ()=>  import('@/views/clasificacion/categorias/CatCategorias.vue') ,
      meta: { ADMIN: true }},
    { path: '/editcategoria',    name: 'editcategoria',     component: ()=>  import('@/views/clasificacion/categorias/EditCategoria.vue') ,
      meta: { ADMIN: true }},    
    
    // DEPARTAMENTOS
    { path: '/catdepartamentos',    name: 'catdepartamentos',     component: ()=>  import('@/views/clasificacion/departamentos/CatDepartamentos.vue') ,
      meta: { ADMIN: true }},
    { path: '/editdepartamento',    name: 'editdepartamento',     component: ()=>  import('@/views/clasificacion/departamentos/EditDepartamento.vue') ,
      meta: { ADMIN: true }},
    { path: '/newdepartamento',     name: 'newdepartamento',      component: ()=>  import('@/views/clasificacion/departamentos/NewDepartamento.vue') ,
      meta: { ADMIN: true }},
    
    // LINEAS
    { path: '/catlineas',    name: 'catlineas',     component: ()=>  import('@/views/clasificacion/lineas/CatLineas.vue') ,
      meta: { ADMIN: true }},
 
    // { path: '/editlinea',    name: 'editlinea',     component: ()=>  import('@/views/clasificacion/lineas/EditLinea.vue') ,
    //   meta: { ADMIN: true }},
    // { path: '/newlinea',     name: 'newlinea',      component: ()=>  import('@/views/clasificacion/lineas/NewLinea.vue') ,
    //   meta: { ADMIN: true }},

    // PROMOS
    { path: '/correos',     name: 'correos',      component: ()=>  import('@/views/promos/Correos.vue') ,
      meta: { ADMIN: true }},
    { path: '/newcorreos',     name: 'newcorreos',      component: ()=>  import('@/views/promos/NuevaListaCorreos.vue') ,
      meta: { ADMIN: true }},
    // { path: '/prueba',     name: 'prueba',      component: ()=>  import('@/views/promos/Prueba.vue') ,
    //   meta: { ADMIN: true }},
    { path: '/prueba',     name: 'prueba',      component: ()=>  import('@/views/Prueba.vue') ,
      meta: { ADMIN: true }},
   
    { path: '/clasproductos',     name: 'clasproductos',      component: ()=>  import('@/views/ClasProductos.vue') ,
      meta: { ADMIN: true }},
    { path: '/operaciones',     name: 'operaciones',      component: ()=>  import('@/views/Operaciones.vue') ,
      meta: { ADMIN: true }},
    { path: '/marketing',     name: 'marketing',      component: ()=>  import('@/views/Marketing.vue') ,
      meta: { ADMIN: true }},
    { path: '/configuracion',     name: 'configuracion',      component: ()=>  import('@/views/Configuracion.vue') ,
      meta: { ADMIN: true }},


    // CATALOGO DE PRUEBAS
    { path: '/consultadocpruebas', name: 'consultadocpruebas', component: ()=> import('@/views/operaciones/CondocsPruebas.vue'),
     meta: { ADMIN: true } },


     // Configurar
    { path: '/configurar', name: 'configurar', component: ()=> import('@/views/configuracion/Configurar.vue'),
     children: [
      { path: '/configcorreos', name: 'configcorreos', component: ()=> import('@/views/configuracion/ConfigCorreos.vue'),
        meta: { ADMIN: true } },
      { path: '/tipocambio', name: 'tipocambio', component: ()=> import('@/views/configuracion/TipoCambio.vue'),
        meta: { ADMIN: true } },
      { path: '/logotipo', name: 'logotipo', component: ()=> import('@/views/configuracion/Logotipo.vue'),
        meta: { ADMIN: true } },
      { path: '/redessoc', name: 'redessoc', component: ()=> import('@/views/configuracion/RedesSoc.vue'),
        meta: { ADMIN: true } },
      { path: '/formato', name: 'formato', component: ()=> import('@/views/configuracion/Formato.vue'),
        meta: { ADMIN: true } },
    ],
     meta: { ADMIN: true } },


     { path: '/confighome', name: 'confighome', component: ()=> import('@/views/hometienda/ConfigHome.vue'),
     children: [
      { path: '/ubicacion', name: 'ubicacion', component: ()=> import('@/views/hometienda/Ubicacion.vue'),
        meta: { ADMIN: true } },
      { path: '/nosotros', name: 'nosotros', component: ()=> import('@/views/hometienda/Nosotros.vue'),
        meta: { ADMIN: true } },
      { path: '/imageneshome', name: 'imageneshome', component: ()=> import('@/views/hometienda/ImagenesHome.vue'),
        meta: { ADMIN: true } },
    ],
     meta: { ADMIN: true } },

    // Vista previa
    { path: '/vistaprevia', name: 'vistaprevia', component: ()=> import('@/views/hometienda/VistaPrevia.vue'),
     meta: { ADMIN: true } },
    //subir video a vimeo


    // Destacados y novedades
    { path: '/noveydes', name: 'noveydes', component: ()=> import('@/views/marketing/noveydes/NoveyDes.vue'),
      meta: { ADMIN: true } },
    { path: '/editseccion', name: 'editseccion', component: ()=>  import('@/views/marketing/noveydes/EditSeccion.vue') ,
      meta: { ADMIN: true } },

    
  ]
})


router.beforeEach( (to, from, next) => {
  // console.log(store.state.Login.datosUsuario.nivel)
  // infica a que ruta voy a acceder
  // matched.some = compara si el meta es libre
  if(to.matched.some(record => record.meta.libre)){
    next()
  }else if(store.state.Login.datosUsuario.nivel){
    if(to.matched.some(record => record.meta.ADMIN)){
      next()
    }
  }else{
    next({
      name: 'Login'
    })
  }
})

export default router 


// Usar este códigoc cuancdo halla niveles


// router.beforeEach( (to, from, next) => {
  
//   if (localStorage.getItem('tlaKey')) {
//     var cToken  = localStorage.tlaKey;
//     next()
//     // NO TIENE TOKEN. cHORIZO
//   }else{
//     localStorage.removeItem("tlaKey")
//     // localStorage.clear();
    
//      var token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1Njc2MTcwNjQsImlkIjoibWFudWVsQHNhaXQuY29tLm14Iiwib3JpZ19pYXQiOjE1Njc2MTM0NjR9.ZZlvQqjH6QX8xLpGm4kmW68NX8vGxC-NE6BWCYJeMYk';
//      localStorage.setItem('tlaKey', token);
//     //  // next ()
//     next({path: '/'});  // next({name: 'Login'});
//   }

//   if(to.matched.some(record => record.meta.libre)){
//     next()

//   }else if(store.state.usuario && store.state.nivel == "ADMIN"){

//     if(to.matched.some(record => record.meta.ADMIN)){
//       next()
//     }

//   }else if(store.state.usuario && store.state.nivel == "USER"){

//     if(to.matched.some(record => record.meta.USER)){
//       next()
//     }

//   }else{
//     //No hay nivel => Mando a Login
//     next({  name: 'Login' })
//   }
// })

// export default router 
