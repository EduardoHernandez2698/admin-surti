import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'
import vuetify from '@/plugins/vuetify';

export default{
	namespaced: true,
	state:{
		login:false,
		datosUsuario:'',
		idusuariosweb: '',
		clientes:[],
		cliente:'',
		vendedor:'',
		tipousuario:'',
	},

	mutations:{
		LOGEADO(state, value){
			state.login = value
		},
		DATOS_USUARIO(state, datosUsuario){
      state.datosUsuario = datosUsuario
		},
		ID_USUARIO(state, idusuariosweb){
			state.idusuariosweb = idusuariosweb
		},

		CLIENTES_USER(state, clientes){
			state.clientes = clientes
		},

		UPDATE_CLIENTE(state, cliente){
			state.cliente = cliente
		},

		INFOVEN(state, infovend){
			state.vendedor = infovend
		},

		TIPOUSUARIO(state, tipousuario){
			state.tipousuario = tipousuario
		},

		SALIR(state){
			state.login = false
			state.datosUsuario = ''
			state.idusuariosweb =  ''
			state.clientes = []
			state.cliente = ''
			state.vendedor = ''
			state.tipousuario= ''
		}
	},

	actions:{
		// Valida si el usario existe en la BD
		validarUser({commit}, usuario){
			return new Promise((resolve, reject) => {
			 // console.log (usuario)
			  Vue.http.post('login', usuario).then(respuesta=>{
			  	return respuesta.json()
			  }).then(respuestaJson=>{
	         // console.log('respuestaJson',respuestaJson)
					if(respuestaJson == null){
						resolve(false) 

					}else{
						resolve(respuestaJson) 
        	}
      	}, error => {
        	reject(error)
      	})
			})
		},

		GetInfoUser({commit, dispatch}, usuario){
			return new Promise((resolve, reject) => {
			  Vue.http.post('v1/usuario', usuario).then(respuesta=>{
	        // console.log('respuestaJsonEMAIL',respuestaJson)
					if(respuesta.body == 'No'){
						resolve(false) 
					}else{
						if(respuesta.body[0].estatus == 1){
							resolve('Valida tu correo') 
						}else if(respuesta.body[0].nivel != 'ADMIN'){
							resolve('No tienes acceso a este nivel')
						}else{
	        		commit('DATOS_USUARIO',respuesta.body[0])
							commit('LOGEADO', true)
							resolve(true)
						}
	      	}
	    	}, error => {
	      	reject(error)
	    	})
			})
		},

		traerCientes({commit}, numcli){
		  Vue.http.get('auth/api/v1/clientebynumcli/' + numcli).then(respuesta=>{
		  	var arr = []
		  	if(typeof(respuesta) == 'object'){
		  		arr.push(respuesta.body)
	      	commit('CLIENTES_USER',arr)
	      	commit('TIPOUSUARIO', 'cliente')
		  	}
	  	}, error => {
	    	console.log(error)
	  	})
		},


		updateCliente({dispatch, commit}, cliente){
			commit('UPDATE_CLIENTE', cliente)
		},

		infovendedor({commit}, idvend){
			Vue.http.get('auth/api/v1/vend/' + idvend).then(response=>{
				// console.log(response.body)
				commit('INFOVEN', response.body)
				commit('TIPOUSUARIO', 'vendedor')
			})
		},

		salirLogin({commit}){
			commit('SALIR')
		},
	},

	getters:{
		getLogeado(state){
		  return state.login
		},
		getdatosUsuario(state){
			return state.datosUsuario
		},

		getidUsuariosWeb(state){
			return state.idusuariosweb
		},

		getClientes(state){
			return state.clientes
		},

		getCliente(state){
			return state.clientes[0]
		},

		getInfoVend(state){
			return state.vendedor
		},

		getModo(state){
			return state.tipousuario
		}
	}
}