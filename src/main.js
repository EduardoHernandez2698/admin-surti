import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import vuetify from './plugins/vuetify';

import VueResource from 'vue-resource'



Vue.config.productionTip = false
Vue.use(VueResource)


Vue.http.options.root = 'http://surtimedix.com/api2/public/'  
// Vue.http.options.root = 'http://localhost:8082/api2/public/'  


console.log(Vue.http.options.root)

// if (!localStorage.getItem('tlKey')) {
//   localStorage.clear();
//   var tokenGen = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1NjIzMjkyMDUsImlkIjoibWFudWVsQHNhaXQubXgiLCJvcmlnX2lhdCI6MTU2MjMyNTYwNX0.-VEL2pk4ihCI9GTSMbye3AlzWjqCMUY4bMJqFmQZYVA';
//   localStorage.tlKey  = JSON.stringify(tokenGen);
// }

Vue.http.interceptors.push((request, next) => {
   // var token = JSON.parse(localStorage.tlaKey)
   // console.log (token)
   request.headers.set('Authorization', 'Bearer '+ localStorage.tlaKey)
  request.headers.set('Accept', 'application/json')
  next()
});



new Vue({
  router,
  store,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')
