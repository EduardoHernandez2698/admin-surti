
export default {

    data () {
      return {
        eventA:{
        	version: "2",
	        dev: "LAPTOP-RAO6OL9E#ManuelGutierrez",
	        usr: "1_MANUEL GUTIERREZ",
	        time: "20191004033118",
	        loc: "9",
	        ref: "N",
	        type: "MODLINEA",
	        src: "tienda en linea ",
        }
     }   
   },  

	methods:{

		sdaddEvent(edata){
			console.log(edata)
			this.eventA.type = edata.type	
			this.eventA.ref  = edata.dbf.val.trim()

		 	var json = {
               event: {
               "_attributes": this.eventA,
               		action:{"_attributes": edata.action,
	               		
                      dbf:{"_attributes": edata.dbf },
                                  
                   		keys: {"_attributes": edata.keys},
	                    
	                    flds: {"_attributes": edata.flds },
                  },
               }
           }

        var convert = require('xml-js');
        var options = {compact: true,  ignoreComment: true, spaces: 0};
        var result = convert.json2xml(json, options);
        console.log(result);    

        var sXML = {
          Evento: result
        }

        console.log('payload', sXML)
          var me = this 
          this.$http.post('auth/api/v1/eventos.add', sXML).then(response => {
             console.log('response', response.body)

            this.snackbar = true ; this.color="blue lighten-3"
            this.text = "El evento se guardo correctamente."
            // setTimeout(function(){ me.$router.push({name: 'catcategorias'}) }, 2000);
          }).catch(function(error){
            console.log(error)
          })
		}
	}	
}
