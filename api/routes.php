<?php 
$app->add(function ($req, $res, $next) {
   $response = $next($req, $res);
   return $response
    ->withHeader('Access-Control-Allow-Origin', '*')
    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Token, Access-Control-Allow-Origin, Accept, Origin, Authorization')
    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

use Slim\Http\Request;
use Slim\Http\Response;

  // -----------------------------------U-S-U-A-R-I-O-S---------------------------------------------------------
  // -----------------------------------------------------------------------------------------------------------
  // -----> CONSULTA PARA ACCEDER AL SISTEMA------------------------------------------------------------------>
  $app->post('/v1/usuario', function(Request $request, Response $response, array $args){
    $datos = $request -> getParsedBody();
    $sth = $this->db->prepare('SELECT  nomuser, email, telefono, celular, numcli, nivel, estatus
                                FROM usuariosweb WHERE email=:email AND password=:password ');
    
    $sth -> bindParam('email'   , $datos['email']);
    $sth -> bindParam('password', $datos['password']);

    //Para ejecutar una consulta 
    $sth->execute();
    $usuarios = $sth->fetchAll();
    if(count($usuarios) > 0){
      echo json_encode($usuarios);
    }else{
      echo ("No");
    }

  });

  // CONSULTAR USUARIO X ID
  // $app->get('/getuserid/{id}', function(Request $request, Response $response, array $args){
  //   $datos = $request -> getParsedBody();
  //   $id = $request->getAttribute('id');
  //   $sth = $this-> db -> prepare('SELECT u.id,u.nombre,u.correo,u.contrasenia,u.telefono,u.nivel,
  //                                        u.registros,u.nivel_general,u.idsuc, s.nomsuc 
  //                                 FROM usuarios u LEFT JOIN sucursales s ON u.idsuc=s.id WHERE u.id =:id');
  //   $sth -> bindParam('id', $id);
  //   $sth -> execute();
  //   $userxid = $sth->fetchAll();
  //   echo json_encode($userxid);
  // });

   // CONSULTAR TODOS LOS USUARIOS
  $app->get('/getusuarios', function(Request $request, Response $response, array $args){
    $sth = $this-> db -> prepare('SELECT * FROM usuarios');
    $sth -> execute();
    $getusuario = $sth->fetchAll();
    echo json_encode($getusuario);
  });

  // INSERTAR USUARIO 
  $app->post('/crearusuario', function(Request $request, Response $response, array $args){
    $datos = $request -> getParsedBody();
    $sth = $this->db->prepare('INSERT INTO usuarios(nombre,correo,telefono,contrasenia,nivel,idsuc)
                                VALUES(:nombre,:correo,:telefono,:contrasenia,:nivel,:idsuc)');

    $sth -> bindParam('nombre'     , $datos['nombre']);
    $sth -> bindParam('correo'     , $datos['correo']);
    $sth -> bindParam('telefono'   , $datos['telefono']);
    $sth -> bindParam('contrasenia', $datos['contrasenia']);
    $sth -> bindParam('nivel'      , $datos['nivel']);
    $sth -> bindParam('idsuc'      , $datos['idsuc']);

    if($sth -> execute()){
      echo "Insertado Correctamente";
    }else{
      echo "Ocurrio un problema";
    }
  });

  // ACTUALIZAR INFROMACION DE USUARIO
  $app->put('/putusuario/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id = $request->getAttribute('id');
    $sth = $this -> db -> prepare('UPDATE usuarios SET nombre=:nombre,correo=:correo,contrasenia=:contrasenia,
                                                       nivel=:nivel,telefono=:telefono,idsuc=:idsuc 
                                    WHERE id=:id;');
    
    $sth -> bindParam('id', $id);
    $sth -> bindParam('nombre'       , $data['nombre']);
    $sth -> bindParam('correo'       , $data['correo']);
    $sth -> bindParam('contrasenia'  , $data['contrasenia']);
    $sth -> bindParam('nivel'        , $data['nivel']);
    $sth -> bindParam('telefono'     , $data['telefono']);
    $sth -> bindParam('idsuc'        , $data['idsuc']);

    $putuser =$sth->execute();
     echo json_encode($putuser);
  });
  
  // BORRAR USUARIO
  $app->delete('/deleteusuario/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id = $request->getAttribute('id');
    
    //cuando no sabes que valor es, se coloca =: cuando si sabes es solo el =
    $sth = $this -> db -> prepare('DELETE FROM usuarios WHERE id=:id');
    $sth -> bindParam('id', $id);
   
    if($sth -> execute()){
      echo "SI";
    }else{
      echo "NO";
    }
  });

  // ACTUALIZAR REGISTROS
  $app->put('/sumregistro/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id   = $request -> getAttribute('id');
    $sth = $this -> db -> prepare('UPDATE usuarios SET  registros =  registros+ 1 WHERE id=:id; ');
    $sth -> bindParam('id', $id);
    $putregistro =$sth->execute();
     echo json_encode($putregistro);
  });

  $app->post('/usuariosxsuc', function(Request $request, Response $response, array $args){
    $datos = $request -> getParsedBody();
    $sth = $this->db->prepare('SELECT u.id, u.nombre, u.correo, s.nomsuc 
                                FROM usuarios u LEFT JOIN sucursales s ON u.idsuc = s.id 
                               WHERE idsuc=:idsuc AND nivel=:nivel');
    $sth -> bindParam('nivel' , $datos['nivel']);
    $sth -> bindParam('idsuc' , $datos['idsuc']);

    $sth->execute();
    $catuser = $sth->fetchAll();
      echo json_encode($catuser);
  });


// -----------------------------------R-E-G-I-S-T-R-O---------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------
  $app->post('/getidxmail', function(Request $request, Response $response, array $args){
    $datos = $request -> getParsedBody();
    $sth = $this->db->prepare('SELECT id FROM usuarios WHERE correo=:correo');
    $sth -> bindParam('correo', $datos['correo']);

    //Para ejecutar una consulta 
    $sth->execute();
    $getid = $sth->fetchAll();
      echo json_encode($getid);
  });

  $app->post('/insertusuarios', function(Request $request, Response $response, array $args){
    $datos = $request -> getParsedBody();
    $sth = $this->db->prepare('INSERT INTO usuarios(nombre,correo,contrasenia,idsuc,nivel)
                                VALUES(:nombre,:correo,:contrasenia,:idsuc,:nivel)');

    $sth -> bindParam('nombre'     , $datos['nombre']);
    $sth -> bindParam('correo'     , $datos['correo']);
    $sth -> bindParam('contrasenia', $datos['contrasenia']);
    $sth -> bindParam('idsuc'      , $datos['idsuc']);
    $sth -> bindParam('nivel'      , $datos['nivel']);

    if($sth -> execute()){
      echo "Insertado Correctamente";
    }else{
      echo "Ocurrio un problema";
    }
  });

// -----------------------------------A-R-T-I-C-U-L-O-S---------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------

  // CONSULTAR MASCOTAS POR USUSARIO ------------------------------------------------------------------------>
  $app->post('/getpets', function(Request $request, Response $response, array $args){
    $datos = $request -> getParsedBody();
    $sth = $this-> db -> prepare('SELECT * FROM mascotas WHERE id_usuario=:id_usuario');
    
    $sth -> bindParam('id_usuario', $datos['id_usuario']); 
    //Para ejecutar una consulta 
    $sth->execute();
    $mascotas = $sth->fetchAll();
      echo json_encode($mascotas);
  });

  // CCONSULTAR TODOS LOS ARTICULOS
  $app->get('/v1/arts.list', function(Request $request, Response $response, array $args){
    $sth = $this-> db -> prepare('SELECT * FROM arts');
    //Para ejecutar una consulta 
    $sth->execute();
    $articulos = $sth->fetchAll();
      echo json_encode($articulos);
  });

  // CCONSULTAR TODOS LOS ARTICULOS
  $app->get('/v1/product.info/{numart}', function(Request $request, Response $response, array $args){
    $data = $request -> getParsedBody(); 
    $numart   = $request->getAttribute('numart');
   
    $sth = $this -> db -> prepare('SELECT * FROM arts WHERE numart=:numart');
    $sth -> bindParam('numart', $numart);
   
      $sth->execute();
      $detallepet = $sth->fetchAll();
      echo json_encode($detallepet);
  });

  // INSERTAR UN ARTICULO
  $app->post('/v1/arts.add', function (Request $request, Response $response, array $args) {
     //recupera los datos enviados
    $datos = $request -> getParsedBody();
    $sth = $this->db->prepare('INSERT INTO  arts 
      ( numart, descrip, unidad, marca, modelo, linea, familia, categoria, numdep, impuesto1, precio1, precio2, precio3, precio4, precio5, activo, preciopub, html)
     VALUES (:numart, :descrip, :unidad, :marca, :modelo, :linea, :familia, :categoria, :numdep, :impuesto1, :precio1, :precio2, :precio3, :precio4, :precio5, :activo, :preciopub, :html)'); 
    
    $sth -> bindParam('numart'      , $datos['numart']);
    $sth -> bindParam('descrip'     , $datos['descrip']);
    $sth -> bindParam('unidad'      , $datos['unidad']);
    $sth -> bindParam('marca'       , $datos['marca']);
    $sth -> bindParam('modelo'      , $datos['modelo']);
    $sth -> bindParam('linea'       , $datos['linea']);
    $sth -> bindParam('familia'     , $datos['familia']);
    $sth -> bindParam('categoria'   , $datos['categoria']);
    $sth -> bindParam('numdep'      , $datos['numdep']);
    $sth -> bindParam('impuesto1'   , $datos['impuesto1']);
    $sth -> bindParam('precio1'     , $datos['precio1']);
    $sth -> bindParam('precio2'     , $datos['precio2']);
    $sth -> bindParam('precio3'     , $datos['precio3']);
    $sth -> bindParam('precio4'     , $datos['precio4']);
    $sth -> bindParam('precio5'     , $datos['precio5']);
    $sth -> bindParam('activo'      , $datos['activo']);
    $sth -> bindParam('preciopub'   , $datos['preciopub']);
    $sth -> bindParam('html'        , $datos['html']);

    if($sth -> execute()){
      echo "SI";
    }else{
      echo "NO";
    }
  });

  // BORRAR MASCOTA POR ID
  $app->delete('/deletepet/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id   = $request->getAttribute('id');
   
    $sth = $this -> db -> prepare('DELETE FROM mascotas WHERE id=:id');
    $sth -> bindParam('id', $id);
   
    if($sth -> execute()){
      echo "SI";
    }else{
      echo "NO";
    }
  });

  // ACTUALIZAR ARTICULO POR NUMART
  $app->put('/v1/arts.update', function (Request $request, Response $response, array $args) {
    $datos = $request -> getParsedBody(); 
    $sth = $this -> db -> prepare('UPDATE arts SET  numart=:numart, descrip=:descrip, unidad=:unidad, marca=:marca, modelo=:modelo, linea=:linea, familia=:familia, categoria=:categoria, numdep=:numdep, impuesto1=:impuesto1, precio1=:precio1, precio2=:precio2, precio3=:precio3, precio4=:precio4, precio5=:precio5, activo=:activo, preciopub=:preciopub, html=:html WHERE numart=:numart ');

    $sth -> bindParam('numart'      , $datos['numart']);
    $sth -> bindParam('descrip'     , $datos['descrip']);
    $sth -> bindParam('unidad'      , $datos['unidad']);
    $sth -> bindParam('marca'       , $datos['marca']);
    $sth -> bindParam('modelo'      , $datos['modelo']);
    $sth -> bindParam('linea'       , $datos['linea']);
    $sth -> bindParam('familia'     , $datos['familia']);
    $sth -> bindParam('categoria'   , $datos['categoria']);
    $sth -> bindParam('numdep'      , $datos['numdep']);
    $sth -> bindParam('impuesto1'   , $datos['impuesto1']);
    $sth -> bindParam('precio1'     , $datos['precio1']);
    $sth -> bindParam('precio2'     , $datos['precio2']);
    $sth -> bindParam('precio3'     , $datos['precio3']);
    $sth -> bindParam('precio4'     , $datos['precio4']);
    $sth -> bindParam('precio5'     , $datos['precio5']);
    $sth -> bindParam('activo'      , $datos['activo']);
    $sth -> bindParam('preciopub'   , $datos['preciopub']);
    $sth -> bindParam('html'        , $datos['html']);


     $putdetalle =$sth->execute();
     echo json_encode($putdetalle);

  });

// -----------------------------------C-A-T-E-G-O-R-I-A-S----------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------

  // CONSULTAR TODOS LOS SERVICIOS
  $app->get('/v1/catego.list', function(Request $request, Response $response, array $args){
    $sth = $this-> db -> prepare('SELECT  c.id, c.numcat, c.nomcat, c.statusweb, c.valdep, d.numdep, d.nomdep 
      FROM catego c INNER JOIN deptos d ON c.valdep = d.id;');
    $sth -> execute();
    $getservicios = $sth->fetchAll();
    echo json_encode($getservicios);
  });

  // CAtegoria por id
  $app->get('/v1/catego.list/{numcat}', function(Request $request, Response $response, array $args){
    $data = $request -> getParsedBody(); 
    $numcat   = $request -> getAttribute('numcat');

    $sth = $this-> db -> prepare('SELECT * FROM catego WHERE numcat=:numcat');
    $sth -> bindParam('numcat' , $numcat);

    $sth -> execute();
    $getcatego = $sth->fetchAll();
    echo json_encode($getcatego);
  });

  // INSERTAR UNA CATEGORIA
  $app->post('/v1/catego.add', function (Request $request, Response $response, array $args) {
     //recupera los datos enviados
    $datos = $request -> getParsedBody();
    $sth = $this->db->prepare('INSERT INTO  catego 
      ( numcat, nomcat, statusweb, valdep, imagen_name, url)
     VALUES (:numcat, :nomcat, :statusweb, :valdep, :imagen_name, :url)'); 
    
    $sth -> bindParam('numcat'      , $datos['numcat']);
    $sth -> bindParam('nomcat'      , $datos['nomcat']);
    $sth -> bindParam('statusweb'   , $datos['statusweb']);
    $sth -> bindParam('valdep'      , $datos['valdep']);
    $sth -> bindParam('imagen_name' , $datos['imagen_name']);
    $sth -> bindParam('url'         , $datos['url']);

    if($sth -> execute()){
      echo "SI";
    }else{
      echo "NO";
    }
  });

  // ACTUALIZAR X ID
  $app->put('/v1/catego.update/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id   = $request -> getAttribute('id');
    $sth = $this -> db -> prepare('UPDATE catego 
      SET nomcat=:nomcat, numcat=:numcat, statusweb=:statusweb, imagen_name=:imagen_name, url=:url WHERE id=:id ');

    $sth -> bindParam('id'          , $id);
    $sth -> bindParam('nomcat'      , $data['nomcat']);
    $sth -> bindParam('numcat'      , $data['numcat']);
    $sth -> bindParam('statusweb'   , $data['statusweb']);
    $sth -> bindParam('imagen_name' , $data['imagen_name']);
    $sth -> bindParam('url'         , $data['url']);

     $putcatego =$sth->execute();
     echo json_encode($putcatego);

  });




  // -----------------------------------D-E-P-A-R-T-A-M-E-N-T-O-S-----------------------------------------------
// -------------------------------------------------------------------------------------------------------------

  // CONSULTAR TODOS LOS DEPARTAMENTOS
  $app->get('/v1/deptos.list', function(Request $request, Response $response, array $args){
    $sth = $this-> db -> prepare('SELECT * FROM deptos');
    $sth -> execute();
    $getservicios = $sth->fetchAll();
    echo json_encode($getservicios);
  });

  // DEPARTAMENTOS POR ID
  $app->get('/v1/deptos.list/{id}', function(Request $request, Response $response, array $args){
    $data = $request -> getParsedBody(); 
    $id   = $request -> getAttribute('id');

    $sth = $this-> db -> prepare('SELECT * FROM deptos WHERE id=:id');
    $sth -> bindParam('id' , $id);

    $sth -> execute();
    $getcatego = $sth->fetchAll();
    echo json_encode($getcatego);
  });

  // INSERTAR UN NUEVO DEPARTAMENTO
  $app->post('/v1/deptos.add', function (Request $request, Response $response, array $args) {
     //recupera los datos enviados
    $datos = $request -> getParsedBody();
    $sth = $this->db->prepare('INSERT INTO  deptos 
      ( numdep, nomdep, statusweb)
     VALUES (:numdep, :nomdep, :statusweb)'); 
    
    $sth -> bindParam('numdep'      , $datos['numdep']);
    $sth -> bindParam('nomdep'     , $datos['nomdep']);
    $sth -> bindParam('statusweb'      , $datos['statusweb']);

    if($sth -> execute()){
      echo "SI";
    }else{
      echo "NO";
    }
  });

  // ACTUALIZAR X ID
  $app->put('/v1/deptos.update/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id   = $request -> getAttribute('id');
    $sth = $this -> db -> prepare('UPDATE deptos 
      SET numdep=:numdep, nomdep=:nomdep, statusweb=:statusweb WHERE id=:id ');

    $sth -> bindParam('id'          , $id);
    $sth -> bindParam('numdep'      , $data['numdep']);
    $sth -> bindParam('nomdep'      , $data['nomdep']);
    $sth -> bindParam('statusweb'   , $data['statusweb']);

     $putdepto =$sth->execute();
     echo json_encode($putdepto);

  });


// --------------------------------S-E-R-V-I-C-I-O-X-T-A-M-A-Ñ-O------------------------------------------------
// -------------------------------------------------------------------------------------------------------------

  $app->get('/servxtamch', function(Request $request, Response $response, array $args){
    $sth = $this-> db -> prepare('SELECT id, nombre, descrip, costo_chico as costo, foto,cantidad FROM servicios ;');
    $sth -> execute();
    $servch = $sth->fetchAll();
    echo json_encode($servch);
  });

  $app->get('/servxtammd', function(Request $request, Response $response, array $args){
    $sth = $this-> db -> prepare('SELECT id, nombre, descrip, costo_mediano as costo, foto,cantidad FROM servicios ;');
    $sth -> execute();
    $servmd = $sth->fetchAll();
    echo json_encode($servmd);
  });

  $app->get('/servxtamgd', function(Request $request, Response $response, array $args){
    $sth = $this-> db -> prepare('SELECT id, nombre, descrip, costo_grande as costo, foto,cantidad FROM servicios ;');
    $sth -> execute();
    $servgd = $sth->fetchAll();
    echo json_encode($servgd);
  });
  
// ------------------------------------------A-C-C-E-S-O-R-I-O-S------------------------------------------------
// -------------------------------------------------------------------------------------------------------------

  $app->get('/getaccesorios', function(Request $request, Response $response, array $args){
    $sth = $this-> db -> prepare('SELECT * FROM accesorios');
    $sth -> execute();
    $accesorios = $sth->fetchAll();
    echo json_encode($accesorios);
  });

// ------------------------------------------A-L-I-M-E-N-T-O-S--------------------------------------------------
// -------------------------------------------------------------------------------------------------------------

  $app->get('/getalimentos', function(Request $request, Response $response, array $args){
    $sth = $this-> db -> prepare('SELECT * FROM alimento');
    $sth -> execute();
    $alimento = $sth->fetchAll();
    echo json_encode($alimento);
  });

// ------------------------------------------H-O-S-P-I-T-A-L----------------------------------------------------
// -------------------------------------------------------------------------------------------------------------

  $app->get('/gethospital', function(Request $request, Response $response, array $args){
    $sth = $this-> db -> prepare('SELECT * FROM hospital');
    $sth -> execute();
    $hospital = $sth->fetchAll();
    echo json_encode($hospital);
  });

// ------------------------------------------S-O-L-I-C-I-T-U-D-E-S----------------------------------------------
// -------------------------------------------------------------------------------------------------------------
  // INSERTAR SOLICITUD
  $app->post('/crearsolicitud', function (Request $request, Response $response, array $args) {
     //recupera los datos enviados
    $datos = $request -> getParsedBody();
    $sth = $this->db->prepare('INSERT INTO  solicitudes ( id_usuario, folio, fecha, total) VALUES (:id_usuario ,:folio, :fecha,:total)'); 
    
    $sth -> bindParam('id_usuario'  , $datos['id_usuario']);
    $sth -> bindParam('folio'       , $datos['folio']);
    $sth -> bindParam('fecha'       , $datos['fecha']);
    $sth -> bindParam('total'       , $datos['total']);

    if($sth -> execute()){
      echo "SI";
    }else{
      echo "NO";
    }
  });

  // INSERTAR SOLICITUD
  $app->post('/insertardetalle', function (Request $request, Response $response, array $args) {
     //recupera los datos enviados
    $datos = $request -> getParsedBody();
    $sth = $this->db->prepare('INSERT INTO  detsoli (folio, id_mascota, modulo, solicitud, cantidad,costo) 
      VALUES (:folio ,:id_mascota, :modulo,:solicitud, :cantidad, :costo)'); 
    
    $sth -> bindParam('folio'       , $datos['folio']);
    $sth -> bindParam('id_mascota'  , $datos['id_mascota']);
    $sth -> bindParam('modulo'      , $datos['modulo']);
    $sth -> bindParam('solicitud'   , $datos['solicitud']);
    $sth -> bindParam('cantidad'    , $datos['cantidad']);
    $sth -> bindParam('costo'       , $datos['costo']);

    if($sth -> execute()){
      echo "SI";
    }else{
      echo "NO";
    }
  });

  // CONSULTAR SOLICITUDES
  $app->post('/getsolicitudes', function(Request $request, Response $response, array $args){
    $datos = $request -> getParsedBody();
    $sth = $this-> db -> prepare('SELECT s.id_usuario, s.folio, s.fecha, s.total, s.estatus, u.nombre 
                                    FROM solicitudes s LEFT JOIN usuarios u ON s.id_usuario = u.id
                                      WHERE s.estatus=:estatus AND s.fecha BETWEEN :fechafrom AND :fechato AND u.idsuc=:idsuc');

    $sth -> bindParam('fechafrom', $datos['fechafrom']);
    $sth -> bindParam('fechato'  , $datos['fechato']);
    $sth -> bindParam('estatus'  , $datos['estatus']);
    $sth -> bindParam('idsuc'    , $datos['idsuc']);


    $sth -> execute();
    $solicitudes = $sth->fetchAll();
    echo json_encode($solicitudes);
  });

  // TRAER LA INFORMACION DE LA SOLICITUD POR FOLIO
  $app->post('/infosoli', function(Request $request, Response $response, array $args){
    $datos = $request -> getParsedBody();
    $sth = $this-> db -> prepare('SELECT s.id  , s.folio, s.fecha, s.total, s.estatus, u.id as id_usuario , u.nombre, u.correo
                                      FROM solicitudes s  LEFT JOIN usuarios u ON s.id_usuario = u.id
                                  WHERE s.folio =:folio ');

    $sth -> bindParam('folio', $datos['folio']);
    $sth -> execute();
    $infosolicitud = $sth->fetchAll();
    echo json_encode($infosolicitud);
  });

  // CONSULTAR SOLICITUDES
  $app->post('/detsoli', function(Request $request, Response $response, array $args){
    $datos = $request -> getParsedBody();
    $sth = $this-> db -> prepare('  SELECT d.id as partida, d.folio, d.modulo, d.solicitud, d.cantidad, d.costo,d.estatus, m.id, m.nombre, 
                                           m.tamanio, m.genero, m.edad, m.estatura, m.foto, t.nombre as raza
                                    FROM detsoli d LEFT JOIN mascotas m ON d.id_mascota = m.id 
                                                   LEFT JOIN tipomascota t ON t.id = m.tipo_mascota 
                                    WHERE d.folio =:folio ');

    $sth -> bindParam('folio', $datos['folio']);
    $sth -> execute();
    $solicitudes = $sth->fetchAll();
    echo json_encode($solicitudes);
  });

  // CAMBIAR ESTATUS DEL DETALLE
  $app->put('/putdetxid/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id   = $request -> getAttribute('id');
    $sth = $this -> db -> prepare('UPDATE detsoli SET estatus = 1 WHERE id=:id');
    $sth -> bindParam('id', $id);
    $putdetalle =$sth->execute();
     echo json_encode($putdetalle);
  });

  // CAMBIAR ESTATUS DEL DETALLE
  $app->put('/putsolixfolio/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id   = $request -> getAttribute('id');
    $sth = $this -> db -> prepare('UPDATE solicitudes SET estatus = 1 WHERE folio=:id');
    $sth -> bindParam('id', $id);
    $putdetalle =$sth->execute();
     echo json_encode($putdetalle);
  });
   // CAMBIAR ESTATUS DEL DETALLE
  $app->put('/cancelasoli/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id   = $request -> getAttribute('id');
    $sth = $this -> db -> prepare('UPDATE solicitudes SET estatus = 2 WHERE folio=:id');
    $sth -> bindParam('id', $id);
    $cancela =$sth->execute();
     echo json_encode($cancela);
  });

// ------------------------------------------S-U-C-U-R-S-A-L-E-S----------------------------------------------
// -----------------------------------------------------------------------------------------------------------
  
  $app->get('/getsucursales', function(Request $request, Response $response, array $args){
    $sth = $this-> db -> prepare('SELECT * FROM sucursales WHERE estatus = 1 ');
    $sth -> execute();
    $sucursales = $sth->fetchAll();
    echo json_encode($sucursales);
  });

// ------------------------- ----------S-O-L-I-C-I-T-U-D-U-S-E-R----------------------------------------------
// -----------------------------------------------------------------------------------------------------------
  $app->post('/getsolixuser', function(Request $request, Response $response, array $args){
    $datos = $request -> getParsedBody();
    $sth = $this-> db -> prepare('SELECT id,folio,fecha FROM solicitudes 
                                    WHERE id_usuario=:id_usuario AND  estatus != 2 ORDER BY id DESC');
    $sth -> bindParam('id_usuario', $datos['id_usuario']);
    $sth -> execute();
    $solixuser = $sth->fetchAll();
    echo json_encode($solixuser);
  });

  $app->delete('/borrarsoli/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id = $request->getAttribute('id');
    $sth = $this -> db -> prepare('DELETE FROM detsoli WHERE id=:id');
    $sth -> bindParam('id', $id);
   
    if($sth -> execute()){
      echo "SI";
    }else{
      echo "NO";
    }
  });

  $app->put('/puttotales/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id   = $request -> getAttribute('id');
    $sth = $this -> db -> prepare('UPDATE solicitudes SET total=:total WHERE folio=:id');
    $sth -> bindParam('id', $id);
    $sth -> bindParam('total', $data['total']);
    $puttotal =$sth->execute();
     echo json_encode($puttotal);
  });


// ------------------------------------------C-H-A-T----------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------
  $app->post('/mensajes', function (Request $request, Response $response, array $args) {
    $datos = $request -> getParsedBody(); 
    $sth = $this-> db -> prepare('SELECT * FROM mensajes 
                                    WHERE id_yo=:id_yo AND id_amigo=:id_amigo OR 
                                          id_yo =:id_amigo AND id_amigo=:id_yo'); 
    $sth -> bindParam('id_yo',        $datos['id_yo']);
    $sth -> bindParam('id_amigo',     $datos['id_amigo']);
    $sth -> execute();
    $mensajes = $sth->fetchAll();
    echo json_encode($mensajes);
  });

  //Mandar un mensaje
  $app->post('/insertmensaje', function (Request $request, Response $response, array $args) {
    $datos = $request -> getParsedBody(); 
    $sth = $this-> db -> prepare('INSERT INTO  mensajes (id_yo, yo, id_amigo, amigo, mensaje, emisor, receptor, estatus, horaenvio) 
                                    VALUES ( :id_yo, :yo, :id_amigo, :amigo, :mensaje, :emisor, :receptor, :estatus, :horaenvio)'); 
    
    $sth -> bindParam('id_yo'    ,  $datos['id_yo']);
    $sth -> bindParam('yo'       ,  $datos['yo']);
    $sth -> bindParam('id_amigo' ,  $datos['id_amigo']);
    $sth -> bindParam('amigo'    ,  $datos['amigo']);
    $sth -> bindParam('mensaje'  ,  $datos['mensaje']);
    $sth -> bindParam('emisor'   ,  $datos['emisor']);
    $sth -> bindParam('receptor' ,  $datos['receptor']);
    $sth -> bindParam('estatus'  ,  $datos['estatus']);
    $sth -> bindParam('horaenvio',  $datos['horaenvio']);

    if($sth -> execute()){
      echo "SI";
    }else{
      echo "NO";
    }
  });

  // TRAER LOS CHATS ACTIVOS PARA ADMIN
  $app->post('/getchats', function (Request $request, Response $response, array $args) {
    $datos = $request -> getParsedBody(); 
    $sth = $this-> db -> prepare('SELECT * FROM mensajes WHERE id_amigo =:id and estatus = 1 '); 
    
    $sth -> bindParam('id',  $datos['id']);
    $sth -> execute();
    $chats = $sth->fetchAll();
    echo json_encode($chats);
  });

  // TRAER TODOS LOS ADMINISTRADORES
  $app->get('/getadmins', function (Request $request, Response $response, array $args) {
    $sth = $this-> db -> prepare('SELECT * FROM usuarios WHERE nivel="ADMIN" '); 
    
    $sth -> execute();
    $adminis = $sth->fetchAll();
    echo json_encode($adminis);
  });

    // ACTUALIZAR MASCOTA POR ID
  $app->put('/estatusmensaje/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id   = $request -> getAttribute('id');
    $sth = $this -> db -> prepare('UPDATE mensajes SET estatus = 0  WHERE idmensaje=:id ');

     $sth -> bindParam('id', $id);
     $estatus =$sth->execute();
     echo json_encode($estatus);
  });

  $app->put('/leido/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id   = $request -> getAttribute('id');
    $sth = $this -> db -> prepare('UPDATE mensajes SET leido = 0  WHERE idmensaje=:id ');

     $sth -> bindParam('id', $id);
     $leido =$sth->execute();
     echo json_encode($leido);

  });
  

//////////////////////// S-U-B-I-R --- A-R-C-H-I-V-O-S ////////////////////
  //api para subir archvio
  $app->post('/documentos', function(Request $request, Response $response, array $args){
    //la direccion donda las vas a guardar
   $uploaddir = "../../imagenes/";
   
   //carga del docuemento
   $uploadfile = $uploaddir . basename($_FILES['file']['name']);
   $error = $_FILES['file']['error'];
   $subido = false;
   $subido = copy($_FILES['file']['tmp_name'], $uploadfile);
    
   if($subido) { 
    echo "El file subio con exito"; 
   } else {
    echo "Se ha producido un error: ".$error;
   }
    
  });

  // Insertar fotografías a la base de datos
  $app->post('/v1/product.image.add', function (Request $request, Response $response, array $args) {
    $datos = $request -> getParsedBody(); 
    $sth = $this-> db -> prepare('INSERT INTO  fotos (numart, url, image_name) 
                                    VALUES ( :numart, :url, :image_name)'); 
    
    $sth -> bindParam('numart'      ,  $datos['numart']);
    $sth -> bindParam('url'         ,  $datos['url']);
    $sth -> bindParam('image_name'  ,  $datos['image_name']);

    if($sth -> execute()){
      echo "SI";
    }else{
      echo "NO";
    }

  });

  // Traer las fotos del articulo
  $app->get('/v1/fotos.list/{numart}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $numart   = $request -> getAttribute('numart');
    $sth = $this-> db -> prepare('SELECT * FROM fotos WHERE numart=:numart '); 

    $sth -> bindParam('numart', $numart);

    $sth -> execute();
    $adminis = $sth->fetchAll();
    echo json_encode($adminis);
  });

  // eliminar fotos
  // BORRAR USUARIO
  $app->delete('/v1/product.image.delete/{id}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $id = $request->getAttribute('id');
    
    //cuando no sabes que valor es, se coloca =: cuando si sabes es solo el =
    $sth = $this -> db -> prepare('DELETE FROM fotos WHERE id=:id');
    $sth -> bindParam('id', $id);
   
    if($sth -> execute()){
      echo "SI";
    }else{
      echo "NO";
    }
  });


  // Eliminar foto de la base de datos
  $app->post('/v1/imagenes.delete/{nomimg}', function (Request $request, Response $response, array $args) {
    $data = $request -> getParsedBody(); 
    $nomimg = $request->getAttribute('nomimg');
    unlink('../../imagenes/' + $nomimg);//acá le damos la direccion exacta del archivo

  });

